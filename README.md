Markdown Format

# Titre principal 
## Titre
### sous Titre

**gras**
*italique*

* Puce 1
* Puce 2

1. Partie 1 
2. Partie 2

* Premier niveau
  * Second niveau  

> Citation 
> Citation

`code incorporé`

``` 
bloc de code
```

![Texte si pas d'image](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.dHRQmoT1JLYGsbzK5jGhnwHaHa%26pid%3DApi&f=1)

[Lien](http://www.youtube.com)

[![texte si pas d'image](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.dHRQmoT1JLYGsbzK5jGhnwHaHa%26pid%3DApi&f=1)](https://www.youtube.com)

---


